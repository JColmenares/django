from django.shortcuts import render,redirect
from django.core.exceptions import ObjectDoesNotExist
from .forms import AutorForm
from .models import Autor

# Create your views here.
def Home(request): # funcion con la petición request para enviar la vista al usuario
    return render(request,'index.html')   #permite renderizar la vista(llama la vista que se quiere mostrar)
                                                #Nota no hace falta agregar la carpeta del template, ya que con el archivo de settings se configura la carpeta onde estara los templatesde las apps y lo selecciona automaticamente

def crearAutor(request):
    if request.method == 'POST': #Pregunta que tipo de envio se esta utilizando
        autor_form = AutorForm(request.POST) #Llama con el envio POST a la class AutoForm de vista automatica en el archivo forms.py de la app
        #print(request.POST)
        if autor_form.is_valid(): #Realiza validaciones basicas automaticamente
            autor_form.save() #Guarda la informacion validada en la base de datos
            return redirect('index') #Redirecciona a otra url luego de guardar en la bd y se coloca como atributo el numbre asignado a la url que se desea redireccionar
    else:
        autor_form = AutorForm() #Se coloca para que cuando se lea por primera vez la pagina cree y cargue el formulario
        #print(autor_form)
    return render(request,'libro/crear_autor.html',{'autor_form':autor_form})

def listarAutor(request):
    #autores = Autor.objects.all()
    autores = Autor.objects.filter(estado = True)
    return render(request,'libro/listar_autor.html',{'autores':autores})

def editarAutor(request,id):
    autor_form = None
    error = None
    var = 1
    try:
        autor = Autor.objects.get(id = id) #Ubica en la BD el id seleccionado
        if request.method == 'GET':
            autor_form = AutorForm(instance = autor)
        else:
            autor_form = AutorForm(request.POST, instance = autor)#Envia la información editada
            if autor_form.is_valid():   #se le aplica validaciones la información
                autor_form.save()       #Y la guarda en la BD
            return redirect('index')    # redirige a index
    except ObjectDoesNotExist as e: #En caso de dar error guarda dicho error en una variable para ser mostrada en el render
        error = e
    return render(request, 'libro/crear_autor.html',{'autor_form':autor_form, 'error':error, 'var':var}) 
    #Realiza el cambio del registro y se redirecciona ala url indicado de no conseguirse error, de lo contrario pinta el error

def eliminarAutor(request,id):
    autor = Autor.objects.get(id = id)  #Ubica el id seleccionado
    if request.method == 'POST':
        #autor.delete()                     #Elimina el registro 
        autor.estado = False                #Permite cambiar el estado para ocultar el registro sin eliminarlo
        autor.save() 
        return redirect('libro:listar_autor')
    return render(request, 'libro/eliminar_autor.html',{'autor':autor})

    